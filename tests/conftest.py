import pytest

from rest_framework.test import APIClient
from django.core.management import call_command
from mailing_manager.models import Message, Mailing, Client as Client_model
from django.utils.timezone import now
from datetime import datetime, timedelta
from mailing_manager.serializers import MessageSerializer, MailingSerializer, ClientSerializer


@pytest.fixture(scope='session')
def django_db_setup(django_db_setup, django_db_blocker):
    # Выполнение миграций перед запуском тестов
    with django_db_blocker.unblock():
        call_command('makemigrations')
        call_command('migrate')


@pytest.fixture(autouse=True)
def enable_db_access_for_all_tests(django_db_setup):
    pass


@pytest.fixture(autouse=True)
def db_clear(django_db_setup, django_db_blocker):
    # Очистка базы данных перед каждым тестом
    with django_db_blocker.unblock():
        call_command('flush', '--noinput')


@pytest.fixture
def default_clients(db_clear, db):
    client_ins = Client_model.objects.create(
        tag="Уфа",
        operator_code='905',
        phone_number='79054501122',
        timezone=now(),
    )
    # return MailingSerializer(instance=client_ins).data


@pytest.fixture
def default_mailing(db_clear, db):
    data = {
        "filter_mobile_operator_code": "936",
        "filter_tag": "Москва",
        "start_time": '2022-10-02T15:59:13.804000+03:00',
        "text": "updated text",
        "end_time": '2033-10-02T15:59:13.804000+03:00'
    }
    data['id'] = Mailing.objects.create(**data).id
    data['messages'] = []
    return data

# @pytest.fixture
# def client():
#     return APIClient