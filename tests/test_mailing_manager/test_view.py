import os
import requests
import json

from rest_framework import status
from mailing_manager.models import Message, Mailing
from datetime import datetime, timedelta
from django.utils import timezone
from mailing_manager.serializers import MessageSerializer, MailingSerializer, ClientSerializer

fbrq_token = os.environ.get("FBRQ_TOKEN")
fbrq_url = os.environ.get("FBRQ_URL")


def test_fbrq_api():
    headers = {
        'Authorization': f'Bearer {fbrq_token}',
        'Content-Type': 'application/json'
    }
    body = {
        "id": 1,
        "phone": 79057837534,
        "text": "message_text"
    }
    response = requests.post(f'{fbrq_url}{1}', json=body, headers=headers)
    assert response.status_code == status.HTTP_200_OK, f"Not working endpoint: {fbrq_url}"


def test_mailing(client, db, default_clients, default_mailing):
    # Проверка получения рассылки по id
    response = client.get(f'/mailing/{default_mailing.get("id")}/')
    assert response.status_code == status.HTTP_200_OK
    assert response.json() == default_mailing

    # Проверка получения всех рассылок
    response = client.get('/mailing/')
    assert response.status_code == status.HTTP_200_OK
    assert len(response.json()) == 1
    assert response.json() == [default_mailing]

    # Проверка обновления рассылки
    data_for_update = {
        "filter_mobile_operator_code": "936",
        "filter_tag": "Москва",
        "start_time": '2022-10-02T15:59:13.804000+03:00',
        "text": "updated text",
        "end_time": '2033-10-02T15:59:13.804000+03:00'
    }
    response = client.patch(f'/mailing/{default_mailing.get("id")}/', data=data_for_update,
                            content_type="application/json")
    assert response.status_code == status.HTTP_200_OK
    data_for_update['id'] = default_mailing.get("id")
    data_for_update['messages'] = []
    assert response.json() == data_for_update

    # Проверка удаления рассылки по id
    response = client.delete(f'/mailing/{default_mailing.get("id")}/')
    assert response.status_code == status.HTTP_204_NO_CONTENT
    assert Mailing.objects.filter(pk=default_mailing.get("id")).first() == None

    data = {
        "filter_mobile_operator_code": "905",
        "filter_tag": "Уфа",
        "start_time": timezone.now() - timedelta(days=1),
        "text": "string",
        "end_time": timezone.now() + timedelta(days=1)
    }
    # Проверка создани рассылки при условии: время начала < текущее время < время завершения
    response = client.post('/mailing/', data=data, content_type="application/json")
    assert response.status_code == status.HTTP_201_CREATED
    assert len(response.json().get("messages")) == 1
    assert Message.objects.filter(mailing_id=response.json().get("id")).first().status == 'Sent'

    # Проверка создани рассылки при условии: время начала,  время завершения < текущее время
    data['start_time'] = datetime.now().date() - timedelta(days=2)
    data['end_time'] = datetime.now().date() - timedelta(days=1)
    response = client.post('/mailing/', data=data, content_type="application/json")
    assert response.status_code == status.HTTP_201_CREATED
    assert len(response.json().get("messages")) == 0

    # Проверка создани рассылки при условии: текущее время < время начала, время завершения
    # status у сообщении = 'Sent' т.к. при тестах celery работает синхронно и задачи выпол-ся сразу
    data['start_time'] = datetime.now().date() + timedelta(days=1)
    data['end_time'] = datetime.now().date() + timedelta(days=2)
    response = client.post('/mailing/', data=data, content_type="application/json")
    assert response.status_code == status.HTTP_201_CREATED
    assert len(response.json().get("messages")) == 1
    assert Message.objects.filter(mailing_id=response.json().get("id")).first().status == 'Sent'

    # Проверка получения статистики
    response = client.get('/mailing/full_info/')
    assert response.status_code == status.HTTP_200_OK
    assert response.json().get('total_mailings') == 3
    assert len(response.json().get("messages")) == 3

    # Проверка получения статистики рассылки по id
    response = client.get(f'/mailing/{1}/info/')
    assert response.status_code == status.HTTP_200_OK
    assert response.json().get('total_message') == 0
