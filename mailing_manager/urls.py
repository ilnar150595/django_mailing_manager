from django.urls import path, include

from rest_framework.routers import SimpleRouter
from rest_framework_nested import routers

from .views import MailingViewSet, MessageViewSet, ClientViewSet

mailing_router = SimpleRouter()
mailing_router.register(r"mailing", MailingViewSet)

client_router = SimpleRouter()
client_router.register(r"client", ClientViewSet)

message_router = routers.NestedSimpleRouter(mailing_router, r'mailing', lookup='mailing')
message_router.register(r'message', MessageViewSet, basename='message')

urlpatterns = [
    path("", include(mailing_router.urls)),
    path("", include(client_router.urls)),
    path("", include(message_router.urls)),
]
