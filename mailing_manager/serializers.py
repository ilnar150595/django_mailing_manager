from rest_framework import serializers
from . import models
from . import signals


class MessageSerializer(serializers.ModelSerializer):
    mailing_id = serializers.IntegerField(
        source='mailing_id.id', read_only=True, allow_null=True)
    created_at = serializers.DateTimeField(read_only=True, allow_null=True)

    class Meta:
        model = models.Message
        fields = ('id', 'mailing_id', 'created_at', 'status')


class MailingSerializer(serializers.ModelSerializer):
    messages = MessageSerializer(many=True, read_only=True)
    filter_mobile_operator_code = serializers.CharField(max_length=3, required=True)
    filter_tag = serializers.CharField(max_length=50, required=False)

    class Meta:
        model = models.Mailing
        fields = "__all__"


class ClientSerializer(serializers.ModelSerializer):
    class Meta:
        model = models.Client
        fields = "__all__"


class InfoSerializer(serializers.Serializer):
    total_message = serializers.IntegerField()
    sent = serializers.IntegerField()
    not_sent = serializers.IntegerField()
    failure = serializers.IntegerField()
