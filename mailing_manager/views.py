from rest_framework.permissions import IsAuthenticated, AllowAny
from rest_framework.viewsets import ModelViewSet, GenericViewSet, ReadOnlyModelViewSet
from .models import Mailing, Message, Client
from .serializers import MailingSerializer, ClientSerializer, MessageSerializer, InfoSerializer
from rest_framework.response import Response
from rest_framework.decorators import action
from drf_spectacular.utils import extend_schema, OpenApiResponse, inline_serializer
from django.db.models import Count, Q
from rest_framework import serializers


class MailingViewSet(ModelViewSet):
    queryset = Mailing.objects.all()
    permission_classes = (AllowAny,)
    http_method_names = ["get", "patch", "delete", "post", ]
    serializer_class = MailingSerializer

    @extend_schema(responses={200: InfoSerializer()})
    @action(detail=True, methods=["get"])
    def info(self, request, pk):
        """
        Get info for the mailing
        """
        serializer = InfoSerializer(data=self.get_info(pk))
        serializer.is_valid()
        return Response(serializer.data)

    @extend_schema(responses={
        200: inline_serializer(
            name='FullInfoResponse',
            fields={
                'total_mailings': serializers.CharField(),
                'messages': InfoSerializer(), }),
    })
    @action(detail=False, methods=["get"])
    def full_info(self, request):
        """
        Get info for all mailings
        """
        mailings = Mailing.objects.values("id")
        content = {
            "total_mailings": len(mailings)
        }
        all_messages = {}

        for mailing in mailings:
            serializer = InfoSerializer(data=self.get_info(mailing["id"]))
            serializer.is_valid()
            all_messages[mailing["id"]] = serializer.data

        content["messages"] = all_messages
        return Response(content)

    def get_info(self, pk) -> dict:
        """
        get dict info for one mailing by id
        """
        res = {}
        results = Message.objects.filter(mailing_id=pk).aggregate(
            total=Count('id'),
            sent=Count('id', filter=Q(status='Sent')),
            not_sent=Count('id', filter=Q(status='Not sent')),
            failure=Count('id', filter=Q(status='Failure')),
        )
        res["total_message"] = results.get('total')
        res["sent"] = results.get('sent')
        res["not_sent"] = results.get('not_sent')
        res["failure"] = results.get('failure')
        return res


class MessageViewSet(ReadOnlyModelViewSet):
    queryset = Message.objects.all()
    permission_classes = (AllowAny,)
    serializer_class = MessageSerializer


class ClientViewSet(ModelViewSet):
    queryset = Client.objects.all()
    permission_classes = (AllowAny,)
    http_method_names = ["get", "patch", "delete", "post", ]
    serializer_class = ClientSerializer
