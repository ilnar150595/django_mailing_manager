import os
import requests

from django_project.celery import app
from .models import Mailing, Message, Client


@app.task(bind=True, retry_backoff=True)
def send_messages(self, mailing_id, message_text):
    """
    sending all messages of the mailing
    """
    fbrq_token = os.environ.get("FBRQ_TOKEN")
    fbrq_url = os.environ.get("FBRQ_URL")
    headers = {
        'Authorization': f'Bearer {fbrq_token}',
        'Content-Type': 'application/json'
    }

    messages = Message.objects.select_related('client_id').filter(mailing_id=mailing_id)

    for message in messages:
        body = {
            "id": message.id,
            "phone": message.client_id.phone_number,
            "text": message_text
        }
        status = 'Failure'
        try:
            response = requests.post(f'{fbrq_url}{message.id}', json=body, headers=headers)
            if response.status_code == 200:
                data = response.json()
                if data['code'] == 0 and data['message'] == 'OK':
                    status = 'Sent'
        finally:
            message.status = status
            message.save()
            message.refresh_from_db()
