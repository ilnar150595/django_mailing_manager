from django.dispatch import Signal
from . import models
from pytz import timezone
from django.utils import timezone
from .tasks import send_messages
from django.db.models.signals import post_save
from django.dispatch import receiver
from django.core.cache import cache
from django_project.celery import app


@receiver(post_save, sender=models.Mailing)
def your_handler(sender, instance, **kwargs):
    """
    Signal for sending messages
    """
    print('**********сработал сигнал для рассылки**************')

    clients = models.Client.objects.all()

    mailing_dict = models.Mailing.objects.values(
        "id", "filter_mobile_operator_code", "filter_tag", "end_time", "start_time").get(pk=instance.pk)

    if mailing_dict.get("filter_mobile_operator_code"):
        clients = clients.filter(operator_code=mailing_dict.get("filter_mobile_operator_code"))
    if mailing_dict.get("filter_tag"):
        clients = clients.filter(tag=mailing_dict.get("filter_tag"))

    # если есть в кэше id этой рассылки то удаляем из селери задачу на эту рассылку и из кэша
    if cache.get(mailing_dict.get("id")):
        app.control.revoke(cache.get(mailing_dict.get("id")), terminate=True)
        print('deleted from cache', cache.get(mailing_dict.get("id")))
        cache.delete(mailing_dict.get("id"))

    # Проверяем является ли время завершения рассылки больше текущего
    if timezone.localtime(timezone.now()) <= mailing_dict.get("end_time"):
        # сохраняем письма
        list_of_clients = []
        for client in clients:
            list_of_clients.append(models.Message(status='Not sent', mailing_id=instance, client_id=client))
        models.Message.objects.bulk_create(list_of_clients)

        # создаем задачу в зависимости от вермени старта задачи
        if mailing_dict.get("start_time") <= timezone.localtime(timezone.now()):
            task = send_messages.apply_async(
                (mailing_dict.get("id"), mailing_dict.get("text")),
                expires=timezone.localtime(mailing_dict.get("end_time")),
            )
            cache_expire = 0
        else:
            task = send_messages.apply_async(
                (mailing_dict.get("id"), mailing_dict.get("text")),
                eta=timezone.localtime(mailing_dict.get("start_time")),
                expires=timezone.localtime(mailing_dict.get("end_time")),
            )
            cache_expire = (mailing_dict.get("start_time") - timezone.localtime(timezone.now())).total_seconds()

        # добавляем id задачи в кэш для данной рассылки и храним до начала старта задачи
        cache.set(mailing_dict.get("id"), task.id, int(cache_expire))
